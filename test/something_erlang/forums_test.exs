defmodule SomethingErlang.ForumsTest do
  use SomethingErlang.DataCase

  alias SomethingErlang.Forums

  describe "threads" do
    alias SomethingErlang.Forums.Thread

    import SomethingErlang.ForumsFixtures

    @invalid_attrs %{thread_id: nil, title: nil}

    test "list_threads/0 returns all threads" do
      thread = thread_fixture()
      assert Forums.list_threads() == [thread]
    end

    test "get_thread!/1 returns the thread with given id" do
      thread = thread_fixture()
      assert Forums.get_thread!(thread.id) == thread
    end

    test "create_thread/1 with valid data creates a thread" do
      valid_attrs = %{thread_id: 42, title: "some title"}

      assert {:ok, %Thread{} = thread} = Forums.create_thread(valid_attrs)
      assert thread.thread_id == 42
      assert thread.title == "some title"
    end

    test "create_thread/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Forums.create_thread(@invalid_attrs)
    end

    test "update_thread/2 with valid data updates the thread" do
      thread = thread_fixture()
      update_attrs = %{thread_id: 43, title: "some updated title"}

      assert {:ok, %Thread{} = thread} = Forums.update_thread(thread, update_attrs)
      assert thread.thread_id == 43
      assert thread.title == "some updated title"
    end

    test "update_thread/2 with invalid data returns error changeset" do
      thread = thread_fixture()
      assert {:error, %Ecto.Changeset{}} = Forums.update_thread(thread, @invalid_attrs)
      assert thread == Forums.get_thread!(thread.id)
    end

    test "delete_thread/1 deletes the thread" do
      thread = thread_fixture()
      assert {:ok, %Thread{}} = Forums.delete_thread(thread)
      assert_raise Ecto.NoResultsError, fn -> Forums.get_thread!(thread.id) end
    end

    test "change_thread/1 returns a thread changeset" do
      thread = thread_fixture()
      assert %Ecto.Changeset{} = Forums.change_thread(thread)
    end
  end
end
