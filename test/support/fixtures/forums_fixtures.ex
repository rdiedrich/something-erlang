defmodule SomethingErlang.ForumsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `SomethingErlang.Forums` context.
  """

  @doc """
  Generate a thread.
  """
  def thread_fixture(attrs \\ %{}) do
    {:ok, thread} =
      attrs
      |> Enum.into(%{
        thread_id: 42,
        title: "some title"
      })
      |> SomethingErlang.Forums.create_thread()

    thread
  end
end
