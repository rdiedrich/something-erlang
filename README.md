# SomethingErlang

Up and running:

  * `mix deps.get`
  * `mix ecto.setup`
  * `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
