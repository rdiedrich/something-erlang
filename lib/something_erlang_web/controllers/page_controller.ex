defmodule SomethingErlangWeb.PageController do
  use SomethingErlangWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def to_forum_path(conn, %{"to" => redir_params} = _params) do
    %{"forum_path" => path} = redir_params

    with [_, thread] <- Regex.run(~r{threadid=(\d+)}, path),
         [_, page] <- Regex.run(~r{pagenumber=(\d+)}, path) do
      redirect(conn,
        to: Routes.thread_show_path(conn, :show, thread, page: page)
      )
    end

    put_flash(conn, :error, "Could not resolve URL")
    render(conn, "index.html")
  end
end
