defmodule SomethingErlangWeb.ThreadLive.Show do
  use SomethingErlangWeb, :live_view
  on_mount SomethingErlangWeb.UserLiveAuth

  alias SomethingErlang.Grover

  require Logger

  @impl true
  def mount(_params, _session, socket) do
    Grover.mount(socket.assigns.current_user)
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id, "page" => page}, _, socket) do
    thread = Grover.get_thread!(id, page |> String.to_integer())

    {:noreply,
     socket
     |> assign(:page_title, thread.title)
     |> assign(:thread, thread)}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     push_redirect(socket,
       to: Routes.thread_show_path(socket, :show, id, page: 1)
     )}
  end

  def post(assigns) do
    ~H"""
    <div class="post">
        <.user info={@author} />
        <article class="postbody">
            <%= raw @article %>
        </article>
        <.toolbar date={@date} />
    </div>
    """
  end

  def user(assigns) do
    ~H"""
    <aside class="userinfo bg-base-100">
        <h3 class="mb-4"><%= @info.name %></h3>
        <div class="title hidden sm:flex flex-col text-sm pr-4">
          <%= raw @info.title %>
        </div>
    </aside>
    """
  end

  def toolbar(assigns) do
    ~H"""
    <div class="sm:col-span-2 text-sm p-2 px-4">
      <%= @date |> Calendar.strftime("%A, %b %d %Y @ %H:%M") %></div>
    """
  end

  def pagination(assigns) do
    %{page: page_number, page_count: page_count} = assigns.thread

    first_page_disabled_button = if page_number == 1, do: " btn-disabled", else: ""
    last_page_disabled_button = if page_number == page_count, do: " btn-disabled", else: ""
    active_page_button = " btn-active"

    prev_button_target = if page_number > 1, do: page_number - 1, else: 1
    next_button_target = if page_number < page_count, do: page_number + 1, else: page_count

    buttons = [
      %{label: "«", page: 1, special: "" <> first_page_disabled_button},
      %{label: "‹", page: prev_button_target, special: "" <> first_page_disabled_button},
      %{label: "#{page_number}", page: page_number, special: active_page_button},
      %{label: "›", page: next_button_target, special: "" <> last_page_disabled_button},
      %{label: "»", page: page_count, special: "" <> last_page_disabled_button}
    ]

    ~H"""
    <div class="navbar my-4 bg-base-200">
        <div class="flex-1"></div>
        <div class="pagination flex-none btn-group grid grid-cols-5">
            <%= for btn <- buttons do %>
                <%= live_redirect class: "btn btn-sm btn-ghost" <> btn.special,
                to: Routes.thread_show_path(@socket, :show, @thread.id, page: btn.page) do %>
                    <%= case btn.label do %>
                        <% "«" -> %><Icons.chevron_left_double /><%= btn.page %>
                        <% "‹" -> %><Icons.chevron_left /><%= btn.page %>
                        <% "›" -> %><%= btn.page %><Icons.chevron_right />
                        <% "»" -> %><%= btn.page %><Icons.chevron_right_double />
                        <% _ -> %><%= btn.page %>
                    <% end %>
                <% end %>
            <% end %>
        </div>
    </div>
    """
  end
end
