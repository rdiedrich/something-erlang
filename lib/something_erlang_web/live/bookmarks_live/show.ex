defmodule SomethingErlangWeb.BookmarksLive.Show do
  use SomethingErlangWeb, :live_view
  on_mount SomethingErlangWeb.UserLiveAuth

  alias SomethingErlang.Grover

  require Logger

  @impl true
  def mount(_params, _session, socket) do
    Grover.mount(socket.assigns.current_user)
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"page" => page}, _, socket) do
    bookmarks = Grover.get_bookmarks!(page |> String.to_integer())

    {:noreply,
     socket
     |> assign(:page_title, "bookmarks")
     |> assign(:bookmarks, bookmarks)}
  end

  @impl true
  def handle_params(_, _, socket) do
    {:noreply,
     push_redirect(socket,
       to: Routes.bookmarks_show_path(socket, :show, page: 1)
     )}
  end
end
