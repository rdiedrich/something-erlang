defmodule SomethingErlang.Repo do
  use Ecto.Repo,
    otp_app: :something_erlang,
    adapter: Ecto.Adapters.Postgres
end
