defmodule SomethingErlang.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Registry, [name: SomethingErlang.Registry.Grovers, keys: :unique]},
      {DynamicSupervisor, [name: SomethingErlang.Supervisor.Grovers, strategy: :one_for_one]},
      # Start the Ecto repository
      SomethingErlang.Repo,
      # Start the Telemetry supervisor
      SomethingErlangWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: SomethingErlang.PubSub},
      # Start the Endpoint (http/https)
      SomethingErlangWeb.Endpoint
      # Start a worker by calling: SomethingErlang.Worker.start_link(arg)
      # {SomethingErlang.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SomethingErlang.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    SomethingErlangWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
