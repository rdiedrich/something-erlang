defmodule SomethingErlang.AwfulApi.Bookmarks do
  require Logger

  alias SomethingErlang.AwfulApi.Client

  def compile(page, user) do
    doc = Client.bookmarks_doc(page, user)
    html = Floki.parse_document!(doc)

    for thread <- Floki.find(html, "tr.thread") do
      parse(thread)
    end
  end

  def parse(thread) do
    %{
      title: Floki.find(thread, "td.title") |> inner_html() |> Floki.raw_html(),
      icon: Floki.find(thread, "td.icon") |> inner_html() |> Floki.raw_html(),
      author: Floki.find(thread, "td.author") |> inner_html() |> Floki.text(),
      replies: Floki.find(thread, "td.replies") |> inner_html() |> Floki.text(),
      views: Floki.find(thread, "td.views") |> inner_html() |> Floki.text(),
      rating: Floki.find(thread, "td.rating") |> inner_html() |> Floki.raw_html(),
      lastpost: Floki.find(thread, "td.lastpost") |> inner_html() |> Floki.raw_html()
    }

    for {"td", [{"class", class} | _attrs], children} <- Floki.find(thread, "td"),
        String.starts_with?(class, "star") == false,
        into: %{} do
      case class do
        <<"title", _rest::binary>> ->
          {:title, children |> Floki.raw_html()}

        <<"icon", _rest::binary>> ->
          {:icon, children |> Floki.raw_html()}

        <<"author", _rest::binary>> ->
          {:author, children |> Floki.text()}

        <<"replies", _rest::binary>> ->
          {:replies, children |> Floki.text() |> String.to_integer()}

        <<"views", _rest::binary>> ->
          {:views, children |> Floki.text() |> String.to_integer()}

        <<"rating", _rest::binary>> ->
          {:rating, children |> Floki.raw_html()}

        <<"lastpost", _rest::binary>> ->
          {:lastpost, children |> Floki.raw_html()}
      end
    end
  end

  defp inner_html(node) do
    node
    |> List.first()
    |> Floki.children()
  end
end
