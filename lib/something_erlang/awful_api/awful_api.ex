defmodule SomethingErlang.AwfulApi do
  require Logger

  alias SomethingErlang.AwfulApi.Thread
  alias SomethingErlang.AwfulApi.Bookmarks

  @doc """
  Returns a list of all posts on page of a thread.

  ## Examples

      iex> t = AwfulApi.parsed_thread(3945300, 1)
      iex> length(t.posts)
      42
      iex> t.page_count
      12
  """
  def parsed_thread(id, page, user) do
    Thread.compile(id, page, user)
  end

  def bookmarks(user) do
    Bookmarks.compile(1, user)
  end
end
