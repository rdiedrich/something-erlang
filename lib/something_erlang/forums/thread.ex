defmodule SomethingErlang.Forums.Thread do
  use Ecto.Schema
  import Ecto.Changeset

  schema "threads" do
    field :thread_id, :integer
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(thread, attrs) do
    thread
    |> cast(attrs, [:title, :thread_id])
    |> validate_required([:title, :thread_id])
  end
end
