defmodule SomethingErlang.Repo.Migrations.UsersAddSadata do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :bbuserid, :string
      add :bbpassword, :string
    end
  end
end
