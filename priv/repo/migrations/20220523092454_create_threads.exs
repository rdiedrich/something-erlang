defmodule SomethingErlang.Repo.Migrations.CreateThreads do
  use Ecto.Migration

  def change do
    create table(:threads) do
      add :title, :string
      add :thread_id, :integer

      timestamps()
    end
  end
end
